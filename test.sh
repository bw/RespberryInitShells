#!/bin/bash

get_docker_test_image() {
    local host_arch=`arch`
    local docker_test_image=
    if [[ "" == "${host_arch}" ]]; then
        echo "get host arch failed"
        exit 1
    fi

    local match_result=`echo "${host_arch}" | grep "arm"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="arm32v7/hello-world"
    fi

    match_result=`echo "${host_arch}" | grep "x86"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="hello-world"
    fi

    match_result=`echo "${host_arch}" | grep "i386"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="hello-world"
    fi

    echo $docker_test_image
}

echo `get_docker_test_image`