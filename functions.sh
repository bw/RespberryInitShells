apt_update() {
    apt update
    ret=$?
    if [[ 0 -ne $ret ]]; then
        echo "$LINENO: apt update is failed"
        echo $ret
    fi
    echo 0
}

print_witch_tag() {
    echo "[$1] $2"
}
