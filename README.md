# 树莓派初始化脚本

#### 项目介绍
树莓派初始化脚本

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)



## Docker安装和运行

1. 在root环境下运行docker_install.sh脚本安装docker
2. 在root环境下运行**add_group_user.sh**脚本添加**新的用户组和用户**
3. 在root环境下运行docker_add_user.sh将**新用户**添加到docker用户组
4. **切换到新的用户环境**运行docker容器。这一步是为了安全，即不在root用户下运行容器



## 树莓派初始化

1. 将树莓派通过网线桥接到电脑上，并登陆ssh
2. 在root环境下运行add_wifi.sh脚本添加WIFI信息，使其可以无线连接
3. 在root环境下运行**add_group_user.sh**脚本添加**新的用户组和用户。**为了安全，具体应用运行在普通用户空间



##工具使用简易教程

### Docker使用

进入容器命令：
```shell
docker exec -ti <CONTAINER ID> /bin/bash
```

构建镜像：
```shell
docker build --force-rm -t [repertory]/<name>[:tag]
```

### Tomcat Docker

可以拉去一个固定的Tomcat镜像，如：tomcat:8.5.31-jre8，命令：
```shell
docker pull tomcat:8.5.31-jre8
```

**启动Tomcat**，并将8080端口映射到宿主机的80端口，命令：

```shell
docker run -d -p 80:8080 tomcat
```

-d：后台运行

-p：设置端口



**run**表示创建并启动镜像，第一次启动的时候用这个命令。下次启动时，运行start命令即可。例子：

```shell
docker start <container name>
```

### ElasticSearch Docker

Docker Hub上的链接被ElasticSearch官方“不推荐”了，所以我找到了官方的docker安装文档，这是链接：
`https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html`。**文档中要重点看的是启动命令。**

docker elasticsearch:6.2.4链接：
```shell
docker pull docker.elastic.co/elasticsearch/elasticsearch:6.2.4
```

开发模式启动运行的命令：
```shell
docker run -d -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.2.4
```
需要开机启动时，可以加上选项"--restart=always "。

ElasticSearch树莓派Docker链接：`https://hub.docker.com/r/charlesyan/rpi-elasticsearch/`，命令：
```shell
docker pull charlesyan/rpi-elasticsearch
```

ElasticSearch Docker源码链接：
```shell
https://github.com/elastic/elasticsearch-docker/tree/6.2
```

安装分词器elasticsearch-analysis-ik
```shell
GITHUB：https://github.com/medcl/elasticsearch-analysis-ik/
安装命令：./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.2.3/elasticsearch-analysis-ik-6.2.3.zip
```

#### Elasticsearch-SQL
GITHUB:https://github.com/NLPchina/elasticsearch-sql

安装命令：
``` shell
./bin/elasticsearch-plugin install https://github.com/NLPchina/elasticsearch-sql/releases/download/6.2.4.0/elasticsearch-sql-6.2.4.0.zip
```
找到Elasticsearch的目录运行上面的命令，若未生效则重启Elasticsearch即可。

### Java Docker

Java8镜像：openjdk:8-jre

