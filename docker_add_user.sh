#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0"  )" && pwd  )"
source $SCRIPT_DIR/functions.sh

help() {
    echo "usage: docker_add_user.sh <user>"
    exit 1
}

if [[ "-h" == "$1" ]] || [[ "--help" == "$1" ]]; then
    help
fi

USER=$1
if [[ "" == "${USER}" ]]; then
    help
fi
print_witch_tag "docker" USER=$USER

usermod -aG docker $USER
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: add user to docker group failed"
    exit $ret
else
    print_witch_tag "docker" "add user to docker group success"
fi
