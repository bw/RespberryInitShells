#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "$0"  )" && pwd  )"
APT_DOCKER_SOURCE_LIST="/etc/apt/sources.list.d/docker.list"

source $SCRIPT_DIR/functions.sh

HOST_ARCH=`arch`

apt_install_docker_dependent() {
    print_witch_tag "docker" "add docker dependent ..."
    apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
    echo $?
}

# Get the Docker signing key for packages
add_the_docker_signing() {
    print_witch_tag "docker" "add docker signing ..."
    curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
    ret=$?
    if [[ 0 -ne $ret ]]; then
        print_witch_tag "docker" "$LINENO: get docker signing failed"
        exit $ret
    else
        print_witch_tag "docker" "add docker signing success"
    fi
}

check_docker_source_list() {
    local docker_source_list=$1
    if [[ -s "$docker_source_list" ]]; then
        echo 0
    else
        echo 1
    fi
}

add_docker_repository_for_raspberry() {
    print_witch_tag "docker" "add docker repository for raspberry ..."
    ret=`check_docker_source_list "$APT_DOCKER_SOURCE_LIST"`
    if [[ 0 -ne $ret ]]; then
        # Add the Docker official repos
        echo "deb [arch=armhf] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" | tee $APT_DOCKER_SOURCE_LIST
        ret=$?
        if [[ 0 -ne $ret ]]; then
            print_witch_tag "docker" "$LINENO: add docker source list failed"
            echo $ret
            return
        else
            print_witch_tag "docker" "add the docker official repos success"
        fi
    else
        print_witch_tag "docker" "docker official repos was existed"
    fi
}

add_docker_repository_for_others() {
    print_witch_tag "docker" "add docker repository for others ..."
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
    echo $?
}

get_docker_test_image() {
    local docker_test_image=
    if [[ "" == "${HOST_ARCH}" ]]; then
        print_witch_tag "docker" "get host arch failed"
        exit 1
    fi

    local match_result=`echo "${HOST_ARCH}" | grep "arm"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="arm32v7/hello-world"
    fi

    match_result=`echo "${HOST_ARCH}" | grep "x86"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="hello-world"
    fi

    match_result=`echo "${HOST_ARCH}" | grep "i386"`
    if [[ "" != "${match_result}" ]]; then
        docker_test_image="hello-world"
    fi

    echo $docker_test_image
}

print_witch_tag "docker" "remove old version docker ..."
apt-get -y remove docker docker-engine docker.io

docker_test_image=`get_docker_test_image`
if [[ "" == "${docker_test_image}" ]]; then
    print_witch_tag "docker" "$LINENO: get docker test image failed"
    exit 1
fi

# 更新
apt_update
print_witch_tag "docker" "apt update success"

# 安装必要的工具
apt_install_docker_dependent
ret=$?
if [[ 0 -ne $ret ]]; then
    apt_update
    apt_install_docker_dependent
fi
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: apt install is failed"
    exit $ret
else
    print_witch_tag "docker" "apt install success"
fi

apt_key_docker=`apt-key list docker`
if [[ "" == "$apt_key_docker" ]]; then
    add_the_docker_signing
fi


if [[ "armv7l" == "${HOST_ARCH}" ]]; then
    ret=`add_docker_repository_for_raspberry`
else
    ret=`add_docker_repository_for_others`
fi

if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: add docker repository failed"
    exit $ret
fi

# Install Docker
apt update
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: apt update failed"
    exit $ret
fi

print_witch_tag "docker" "install docker..."
apt install -y docker-ce
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: apt install docker-ce failed"
    exit $ret
else
    print_witch_tag "docker" "install docker success"
fi

systemctl enable docker
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: enable docker failed"
    exit $ret
else
    print_witch_tag "docker" "enable docker success"
fi

systemctl start docker
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: start docker failed"
    exit $ret
else
    print_witch_tag "docker" "start docker success"
fi

docker run --rm $docker_test_image
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "docker" "$LINENO: test docker failed"
    exit $ret
else
    print_witch_tag "docker" "test docker success"
fi

docker_group_info=`cat /etc/group | grep -E "^docker:"`
if [[ "" == "$docker_group_info" ]]; then
    print_witch_tag "docker" "docker group not exist, create docker group"
    groupadd docker
    ret=$?
    if [[ 0 -ne $ret ]]; then
        print_witch_tag "docker" "$LINENO: add docker group is failed"
        exit $ret
    else
        print_witch_tag "docker" "add docker group is successful"
    fi
else
    print_witch_tag "docker" "docker group is existed"
fi

print_witch_tag "docker" "install docker finish"
