#!/bin/bash

# set -x

SCRIPT_DIR="$( cd "$( dirname "$0"  )" && pwd  )"

source $SCRIPT_DIR/functions.sh

apt_update

apt install -y python-setuptools python-dev flake8
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "InitEnv" "$LINENO: install failed"
    exit $ret
else
    echo "install success"
fi


easy_install pip
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "InitEnv" "$LINENO: easy_install install failed"
    exit $ret
else
    echo "easy_install install success"
fi

pip install jinja2 jinja2-cli
ret=$?
if [[ 0 -ne $ret ]]; then
    print_witch_tag "InitEnv" "$LINENO: pip install jinja2 failed"
    exit $ret
else
    echo "pip install jinja2 success"
fi
